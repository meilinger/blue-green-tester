import os
import json
from flask import Flask, Response

app = Flask(__name__)

@app.route('/')
def index():
    return 'HI'  # Response(json.dumps(os.environ.data, indent=2), mimetype='application/json')
    
if __name__ == '__main__':
    app.run(debug=True)
